package com.example.demo.controller;

import com.example.demo.service.EmailNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailNotificationController {

    private final EmailNotification emailNotification;

    @Autowired
    public EmailNotificationController(EmailNotification emailNotification) {
        this.emailNotification = emailNotification;
    }

    @GetMapping(value = "/sent")
    public void sendEmailApi() {
        this.emailNotification.SendMail("Gabi");
    }
}
