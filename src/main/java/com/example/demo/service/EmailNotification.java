package com.example.demo.service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class EmailNotification {
    @Value("${spring.sendgrid.api-key}")
    private String apiKey;
    @Value("${spring.sendgrid.templateId}")
    private String templateId;
    private static final String fromEmailId = "ghislaincabrel.kemfang@gmail.com";
    private static final String senderName = "Cabrel";
    private static final String toEmailId = "ghislainck7@gmail.com";

    private final SendGrid sendGrid;

    @Autowired
    public EmailNotification(SendGrid sendGrid) {
        this.sendGrid = sendGrid;
    }

    public boolean SendMail(String name) {
        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();

        try {
            Mail dynamicTemplate = buildDynamicTemplate(name);
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(dynamicTemplate.build());
            Response response = sg.api(request);

            if (response.getStatusCode() == 202)
                log.info("sent");
                return true;

        } catch (IOException ex) {
            // TODO
            log.info(String.valueOf(ex));
        }
        return false;
    }

    // API V3 Dynamic Template implementation
    public Mail buildDynamicTemplate(String name) throws IOException {

        Mail mail = new Mail();
        Email fromEmail = new Email();
        fromEmail.setName(senderName);
        fromEmail.setEmail(fromEmailId);
        mail.setFrom(fromEmail);
        mail.setTemplateId(templateId);

        Personalization personalization = new Personalization();
        //This is the value that will be passed to the dynamic template in SendGrid
        personalization.addDynamicTemplateData("name", name);
        personalization.addTo(new Email(toEmailId));
        mail.addPersonalization(personalization);

        return mail;
    }
}
